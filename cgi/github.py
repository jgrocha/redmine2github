#!/usr/bin/python2
# -*- coding: UTF-8 -*-

# Import modules for CGI handling 
import cgi, cgitb 
import csv
import datetime

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
user_uid  = form.getvalue('uid')
user_email  = form.getvalue('mail')
user_name  = form.getvalue('name')
user_github  = form.getvalue('user_github')

# http://localhost/cgi-bin/github.py?id=1122&name=Jorge%20Rocha&mail=jgr@di.uminho.pt
# http://localhost/cgi-bin/github.py?id=1122&name=Jorge%20Rocha&mail=jgr@di.uminho.pt&github=jgrocha

print "Content-type:text/html\r\n\r\n"

print """
<html>
<head>
<title>QGIS issue tracker migration</title>
</head>
<body>
<img src="https://www.qgis.org/en/_static/logo.png" />
"""

print """<h3>Migrating issues to GitHub</h3>
<p>We would like to migrate our issue tracker to GitHub.</p>
<p>The current platform, based on Redmine 2.5.2, is becoming obsolete and hard to upgrade. We already use GitHub as our code repository. Having the issue tracker on the same platform enables better integration. 
GitHub also has better tools to improve issue reporting, triaging and management.</p>
<h3>Mapping user accounts</h3>
<p>When migrating issues and related comments, we would like to associate the Redmine user with a GitHub user account.
If you provide your GitHub account, your previous activity (issues and comments) will have a link to your GitHub account.</p>"""

further = """<h3>Further information</h3>
<p>All the migration process has been discussed within the QGIS community for the last 3 years. 
To make the process clear, the migration proposal was published as 
a <a href="https://github.com/qgis/QGIS-Enhancement-Proposals/issues/141">QGIS Enhancement Proposal</a></p>"""


if user_uid and user_name and user_email:
  dataform = """<h3>Form</h3>
<p>Fill your GitHub acount. Do not provide the email associated with your GitHub account (<i>although you can log in using the account name or the email</i>).</p>
<form method="get" action="#">
<input type="hidden" name="uid" value=\"""" + user_uid + """\" />
<table border="0">
<tr><td>Name</td><td><input type="text" name="name" value=\"""" + user_name + """\" readonly/></td></tr>
<tr><td>Email</td><td><input type="email" name="mail" value=\"""" + user_email + """\" readonly/><br /></td></tr>
<tr><td>GitHub account</td><td><input type="text" name="user_github" /><br /></td></tr>
<tr><td></td><td><input type="submit" value="Submit" /></td></tr>
</table>
</form>

<p>If you don't have a GitHub account or if you prefer do not share it, you still be listed as the original author. 
The history of contributions will be preserved.</p>"""

  if user_github:
    with open("github.csv", "a") as myfile:
      github_writer = csv.writer(myfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
      github_writer.writerow([user_uid, user_name, user_email, user_github, datetime.datetime.now().isoformat()])
    print "<h2>Thank you</h2>"
    print "<p>Your github account <b>%s</b> was recorded.</p>" % (user_github)
    print "<p>Thank you <b>%s</b> for your cooperation.</p>" % (user_name)
  else:
    print dataform
else:
  print """
<h2>Error</h2>
<p>Error open form. Please seu the link provided. Check if the link was broken or splitted in lines.</p>
"""

print further

print """</body>
</html>"""