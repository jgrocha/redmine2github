#!/bin/bash

#get line count
linecount=$(cat issueusers.csv  | wc -l)
#set counting variable
counter=1
#loop through file
while read line; do 
    echo $line;
    #split each line into an array by comma
    IFS=';' read -ra arr <<< "$line"; 
    #echo $arr;
    #set  array values to variables
    uid=${arr[0]}; 
    name=${arr[1]};
    mail=${arr[2]}; 
    encoded_name=$(python2 -c "import urllib; print urllib.quote('''$name''')")
    link=$(printf "https://www.qgis.org/cgi-bin/github.py?uid=%s&name=%s&mail=%s" "$uid" "$encoded_name" "$mail")
    subject="Migrating QGIS issue tracker to GitHub"; 
    intro="
We would like to migrate our issue tracker to GitHub.

The current platform, based on Redmine 2.5.2, is becoming obsolete and hard to upgrade. We already use GitHub as our code repository. Having the issue tracker on the same platform enables better integration. 
GitHub also has better tools to improve issue reporting, triaging and management.

When migrating issues and related comments, we would like to associate the Redmine user with a GitHub user account.
If you provide your GitHub account, your previous activity (issues and comments) will have a link to your GitHub account.

All the migration process was published at: https://github.com/qgis/QGIS-Enhancement-Proposals/issues/141

To provide your GitHub account, please fill the following form:
$link

If you don't have a GitHub account or if you prefer do not share it, you still be listed as the original author. 
The history of contributions will be preserved.

Best regards from te QGIS issue tracker team.
";

    #create body
    body=$(printf "Dear %s\n\n%s%s" "$name" "$intro")
    #echo progress
    # echo "Sending email $counter of $linecount: Subject: $subject, Body: $body"
    echo "Sending email $counter of $linecount: Name: $name, Link: $link"
    #send email
    echo "$body" | mail -s "$subject" $mail -- -f redmine@qgis.org 
    #advance counter
    counter=$(( $counter +1 ))
    #sleep for a second
    sleep 1
done < issueusers.csv 
