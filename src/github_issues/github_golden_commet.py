import requests
import json
import time

from jinja2 import Template
from jinja2 import Environment, PackageLoader

if __name__ == '__main__':
    SRC_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(SRC_ROOT)

try:
    from utils.msg_util import *
    from github_issues.md_translate import translate_for_github
except:
    raise


class Issue:
    """ Data needed to represent an issue internally"""

    def __init__(self, issue_number, issue_id, issue_url):
        self.id = issue_id
        self.number = issue_number
        self.html_url = issue_url


class GitHubIssueHelper:
    """ Use GitHub's issue import api to create issues without triggering
        api abuse limits so easily
    """

    def __init__(self, gim=None, **config):
        self._user = config['user']
        self._repo = config['repo']
        self._token = config['password']

        self._auth = (self._user, self._token)
        self.issue_url = 'https://api.github.com/repos/%s/%s/import/issues' % (self._user, self._repo)

        self.jinja_env = Environment(loader=PackageLoader('github_issues', 'templates'))

        self.imported = 0
        self.gim = gim

    def create(self, issue_dict, journals, attachments):
        issue_data = {}
        issue_data['issue'] = issue_dict
        issue_data['comments'] = self.gim.process_journals(journals, attachments)
        # print(issue_data)

        headers = {'Accept': 'application/vnd.github.golden-comet-preview+json'}
        req = requests.post(self.issue_url, data=json.dumps(issue_data), auth=self._auth, headers=headers)

        response = None
        if req.status_code in [200, 201]:
            self.imported += 1
            issue_url = json.loads(req.text)['issue_url']
            issue_id = int(issue_url.split('/')[-1])
            response = Issue(self.imported, issue_id, issue_url)
        elif req.status_code in [202]:
            response = self.check_import_loop(req.text, 1)
        else:
            print(req.status_code)
            print(req.text)
            while response is None:
                time.sleep(2)
                headers = {'Accept': 'application/vnd.github.golden-comet-preview+json'}
                req = requests.post(self.issue_url, data=json.dumps(issue_data), auth=self._auth, headers=headers)
                if req.status_code in [200, 201]:
                    self.imported += 1
                    issue_url = json.loads(req.text)['issue_url']
                    issue_id = int(issue_url.split('/')[-1])
                    response = Issue(self.imported, issue_id, issue_url)
                elif req.status_code in [202]:
                    response = self.check_import_loop(req.text, 1)

        # time.sleep(1)

        return response

    def check_import_loop(self, import_response, check_interval):
        check_data = json.loads(import_response)
        response = None

        while response is None:
            try:
                time.sleep(check_interval)
                msg('Checking issue import status')
                headers = {'Accept': 'application/vnd.github.golden-comet-preview+json'}
                req = requests.get(check_data['url'], auth=self._auth, headers=headers)
                if req.status_code in [200, 201]:
                    resp_data = json.loads(req.text)
                    if 'status' in resp_data and resp_data['status'] == 'pending':
                        msg('import still processing ...')
                    elif 'status' in resp_data and resp_data['status'] == 'imported':
                        self.imported += 1
                        issue_url = json.loads(req.text)['issue_url']
                        issue_id = int(issue_url.split('/')[-1])
                        response = Issue(self.imported, issue_id, issue_url)
                    else:
                        print(resp_data)
                else:
                    print(req.status_code)
                    print(req.text)
            except:
                raise

        return response


class GitHubIssueImporter:
    """ doc """

    def __init__(self, user_map_helper=None, **config):
        self._user = config['login']
        self._repo = config['repo']
        self._token = config['password']

        self.issues = GitHubIssueHelper(user_map_helper, **config)
